﻿using AttachedMode.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AttachedMode.Data
{
    public class GruppaDataAccess:DbDataAccess<Gruppa>
    {
        public Gruppa Gruppa { get; private set; } = new Gruppa();
    }
}
