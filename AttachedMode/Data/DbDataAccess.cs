﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Reflection;
using System.Text;

namespace AttachedMode.Data
{
    public abstract class DbDataAccess<T>
    {
        protected readonly SqlConnection connection;
        public DbDataAccess()
        {
            connection = new SqlConnection();
            connection.ConnectionString = "Server=DESKTOP-0V33MK0\\MSSQLSERVER01; Database = AttachedMode; Trusted_Connection=true;";
            connection.Open();
        }
        public void Dispose()
        {
            connection.Close();
        }
        public void CreateTable(T entity)
        {
            Dictionary<string, string> typeDictionary = new Dictionary<string, string>();
            typeDictionary.Add("String", "nvarchar");
            typeDictionary.Add("Double", "float");
            typeDictionary.Add("DateTime", "date");
            typeDictionary.Add("Int32", "int");

            var createSqlScript = $"Create table {entity.GetType().Name.ToLower()} \n(";

            foreach (var property in entity.GetType().GetProperties())
            {
                if (typeDictionary.ContainsKey(property.PropertyType.Name))
                {
                    createSqlScript += $"{property.Name} ";
                    switch (property.PropertyType.Name)
                    {
                        case "Int32":
                            createSqlScript+= $"{typeDictionary[property.PropertyType.Name]}";
                            if (property.Name.ToLower() == "id")
                            {
                                createSqlScript += " primary key identity";
                            }
                            createSqlScript += " not null,";
                            break;
                        case "String":
                            createSqlScript += $"{typeDictionary[property.PropertyType.Name]}(max)";
                            createSqlScript += " null,";
                            break;
                        case "Double":
                            createSqlScript += $"{typeDictionary[property.PropertyType.Name]}";
                            createSqlScript += " not null,";
                            break;
                        case "DateTime":
                            createSqlScript += $"{typeDictionary[property.PropertyType.Name]},";
                            break;
                    }
                    createSqlScript +="\n";
                }

            }

            createSqlScript += ")";

            using (var command = new SqlCommand(createSqlScript, connection))
            {
                command.ExecuteNonQuery();
            }
        }
    }
}
