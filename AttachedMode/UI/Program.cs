﻿using AttachedMode.Data;
using AttachedMode.Models;
using System;

namespace AttachedMode.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            var gruppaData = new GruppaDataAccess();
            gruppaData.CreateTable(gruppaData.Gruppa);
        }
    }
}
