﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AttachedMode.Models
{
    public class Gruppa
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
